#ifndef structs_h
#define structs_h

	struct global{
		//unsigned int header : 192;
		unsigned int magicNum : 32;
		unsigned int majVer : 16;
		unsigned int minVer : 16;
		unsigned int gmtTime : 32;
		unsigned int accuracy : 32;
		unsigned int snapLen : 32;
		unsigned int linkLayer : 32;
	};

	struct pktHead{
		//unsigned int header : 128;
		unsigned int secs;
		unsigned int micro;
		unsigned int lenBytes;
		unsigned int lenWire;
	};

	struct ethernet{
		//unsigned int header : 112;
		unsigned int destMac1 : 32;
		unsigned int destMac2 : 16;
		unsigned int sourceMac1 : 32;
		unsigned int sourceMac2 : 16;
		unsigned int nextType : 16;
	};

	struct ipv4{
		//unsigned int header : 160;
		unsigned int ipv41 : 32;
		unsigned int ipv42 : 32;
		unsigned int ipv43 : 32;
		unsigned int ipv44 : 32;
		unsigned int ipv45 : 32;
	};

	struct udp{
		//unsigned int header : 64;
		unsigned int sourceP : 16;
		unsigned int destP : 16;
		unsigned int len: 16;
		unsigned int checksum: 16;
	};

	union altitudeU{
		float altitude;
		char altArr[4];
	};

	union latitudeU{
		double latitude;
		char latArr[8];
	};
	union longitudeU{
		double longitude;
		char lonArr[8];
	};

	struct gps{
		union latitudeU lat;
		union longitudeU lon;
		union altitudeU alt;
	};

	union batteryU{
		double battery;
		char batArr[8];
	};
	struct status{
		union batteryU bat;
		unsigned int glu;
		unsigned int cap;
		unsigned int omo;
	};

	struct command{
		unsigned int cmd;
		unsigned int para;
	};

	struct message{
		char msg[20];
	};
	union medPayload{
		struct gps gpss;
		struct status stat;
		struct command cmd;
		struct message msg;
	};

	struct meditrik{
		unsigned int version : 4;
		unsigned int seqID : 9;
		unsigned int type : 3;
		unsigned short totalLen;
		unsigned int sourceID;
		unsigned int destID;
		union medPayload payload;
	};

	struct bitAssign{
		unsigned int typ : 3;
		unsigned int sequ : 9;
		unsigned int vers : 4;
	};

	union verTypeSeq{
		short bytes2;
		struct bitAssign BA;
	};


	int initGlobal(struct global* gHead); // structs.h
	int initPktHead(struct pktHead* pHeadPtr);
	int initEthHead(struct ethernet* eHead);
	int initIpHead(struct ipv4* ipHead);
	int initUdpHead(struct udp* uHead);
	int initMedHead(struct meditrik* mPoint);

#endif