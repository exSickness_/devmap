#ifndef parseMed_h
	#define parseMed_h

#include "structs.h"
#include "graph.h"

int getMedStart(int protocol);
int getMedHead(unsigned int* buf,struct meditrik* a,int medStart);
int getMedType(unsigned int* buf, struct meditrik* a,int medStart, struct fields* f);

int getTotalBytes(int fd);
int getPacketLen(int fd, int offset );
int readPacket(int fd, unsigned int* buf, int offset, int pLen);

int checkProtocol(unsigned int* buf);
void printBuf(unsigned int* buf, size_t sz);
struct fields* createField(void);

#endif