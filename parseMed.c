#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <ctype.h>

#include "parseMed.h"


int getMedHead(unsigned int* buf,struct meditrik* a,int medStart)
{
	if((a == NULL) || (buf == NULL) || (medStart == '\0'))
	{
		fprintf(stderr,"Null parameter passed in.\n");
		return(-1);
	}
	unsigned char temp = buf[medStart];
	temp >>= 4;
	a->version = temp;
	// printf("Version: %d\n", a->version);
	if(a->version != 1) {
		fprintf(stderr,"Invalid version number.\n");
		return(-1);
	}

	unsigned char tempType = buf[medStart+1];
	unsigned char mask = ~7;
	tempType |= mask;
	tempType ^= mask;
	a->type = tempType;
	//printf("Type: %d\n",a->type);

	unsigned int tempSeq = buf[medStart];
	unsigned int maskSeq = ~15;
	tempSeq |= maskSeq;
	tempSeq ^= maskSeq;
	tempSeq <<= 5;
	unsigned char tempSeq2 = buf[medStart +1];
	tempSeq2 >>= 3;
	tempSeq += tempSeq2;
	a->seqID = tempSeq;
	// printf("Seq#: %d\n",a->seqID);

	unsigned int tempTotLen = 0;
	tempTotLen += buf[medStart +2];
	tempTotLen <<= 8;
	tempTotLen += buf[medStart +3];
	a->totalLen = tempTotLen;
	// printf("Total Length: %x\n",a->totalLen);

	unsigned int tempSourceD;
	tempSourceD = buf[medStart +4];
	for(int i = 5;i<=7; i++)
	{
		tempSourceD <<= 8;
		tempSourceD += buf[medStart +i];
	}
	a->sourceID = tempSourceD;
	// printf("From: %d\n",a->sourceID); // Source Device ID


	unsigned int tempDestD = buf[medStart +8];
	for(int i = 9;i<=11; i++) // Loop 4 times
	{
		tempDestD <<= 8;
		tempDestD += buf[medStart +i];
	}
	a->destID = tempDestD;
	// printf("To: %d\n",a->destID); // Destination Device ID

	return(0);
}

// =========================================================================================================

int getMedType(unsigned int* buf, struct meditrik* a, int medStart, struct fields* f)
{

//		0 - GET STATUS
//		1 - SET GLUCOSE
//		2 - GET GPS
//		3 - SET CAPSAICIN
//		4 - RESERVED
//		5 - SET OMORFINE
//		6 - RESERVED
//		7 - REPEAT

	if((a == NULL) || (buf == NULL) || (medStart == '\0'))
	{
		fprintf(stderr,"Null parameter passed in.\n");
		return(-1);
	}

	medStart +=12;

	if(a->type == 0) // Status of device
	{
		union batteryPwr {
			unsigned char tempStat1[8];
			double pwr;
		};

		union batteryPwr pwrforbats;

		for(int i = 0;i< 8; i++) {
			pwrforbats.tempStat1[i] = buf[medStart+i];
		}

		f->pow = pwrforbats.pwr;
		// printf("Battery Power: %.2f%%\n",pwrforbats.pwr * 100);

		unsigned int tempStatD = buf[medStart+8]; // 94 + 8
		tempStatD <<= 8;
		tempStatD += buf[medStart +9];
		// printf("Glucose: %d\n",tempStatD);

		unsigned int tempStatD2 = buf[medStart +10];
		tempStatD2 <<= 8;
		tempStatD2 += buf[medStart +11];
		// printf("Capsaicin: %d\n",tempStatD2);

		unsigned int tempStatD3 = buf[medStart +12];
		tempStatD3 <<= 8;
		tempStatD3 += buf[medStart +13];
		// printf("Omorfine: %d\n",tempStatD3);
	}
	else if(a->type == 2) // GPS Data
	{
		union latLong {
			unsigned char tempStat1[8];
			double field;
		};

		union latLong latLongStuff;

		for(int i = 0;i< 8; i++) {
			latLongStuff.tempStat1[i] = buf[medStart+i];
		}

		f->lon = latLongStuff.field;

		medStart += 8;
		for(int i = 0;i< 8; i++) {
			latLongStuff.tempStat1[i] = buf[medStart+i];
		}

		f->lat = latLongStuff.field;
		if(f->lon > 180 || f->lon < 0) {
			fprintf(stderr,"Device #%d has an invalid longitude!(%.0f)\n",f->dev,f->lon);
			return(-1);
		}
		if(f->lat > 90 || f->lat < -90) {
			fprintf(stderr,"Device #%d has an invalid latitude!(%.0f)\n",f->dev,f->lat);
			return(-1);
		}
		// printf("Latitude: %f deg N\n",f->lat);
		// printf("Longitude: %f deg W\n",f->lon);

		union altitude{
			char spot[4];
			float alt;
		};

		union altitude tempAlt;

		medStart += 8;
		for(int i = 0;i< 4; i++) {
			tempAlt.spot[i] = buf[medStart+i];
		}

		f->alt = tempAlt.alt * 6;
		// printf("Altitude: %.2f ft.\n",f->alt );
	}
	else {
		return(0);
	}

	return(0);
}


struct fields* createField(void)
{
	struct fields* f = malloc( sizeof(*f) );
	if(f)
	{
		f->lat = -1;
		f->lon = -1;
		f->alt = -1;
		f->pow = -1;
		f->dev = -1;
	}

	return(f);
}

int checkProtocol(unsigned int* buf)
{
	if( buf[12] == 0x08 ) { // ipv4 packet
		if(buf[23] == 0x11) { // udp packet
			if( buf[34] == 0xde && buf[35] == 0xad ) { // correct source port
				if(buf[36] == 0xde && buf[37] == 0xad ) { // correct dest port
					return(4);
				}
				else {
					fprintf(stderr,"Incorrect destination port!\n");
					return(-1);
				}
			}
			else {
				fprintf(stderr,"Incorrect source port!\n");
				return(-1);
			}
		}
		else {
			fprintf(stderr,"Incorrect layer 4 protocol!\n");
			return(-1);
		}
	}
	else if( buf[12] == 0x86 ) {
		if(buf[20] == 0x11) { // udp packet
			if( buf[54] == 0xde && buf[55] == 0xad ) { // correct source port
				if(buf[56] == 0xde && buf[57] == 0xad ) { // correct dest port
					return(6);
				}
				else {
					fprintf(stderr,"Incorrect destination port!\n");
					return(-1);
				}
			}
			else {
				fprintf(stderr,"Incorrect source port!\n");
				return(-1);
			}
		}
		else {
			fprintf(stderr,"Incorrect layer 4 protocol!\n");
			return(-1);
		}
	}
	return(-1);
}

int getMedStart(int protocol)
{
	int start = 0;

	start += 14; // Ethernet Header

	if(protocol == 4) { // IPv4 Header
		start += 20;
	}
	else if(protocol == 6) { // IPv6 Header
		start += 40;
	}
	else { // Unknown protocol
		fprintf(stderr,"Unable to get to meditrik header.\n");
		return(-1);
	}

	start += 8; // UDP Header

	return(start);
}


void printBuf(unsigned int* buf, size_t sz)
{
	for(size_t i = 0; i < sz; i++)
	{
		if(i % 16 == 0) {
			printf("\n");
		}
		printf("%02x ",buf[i]);
	}
}

int readPacket(int fd, unsigned int* buf, int offset, int pLen)
{
	if(lseek(fd,offset,SEEK_SET) == -1) {
		fprintf(stderr,"Error seeking to read file into buffer.\n");
		return(-1);
	}

	for(int i = 0 ; i < pLen; i++)
	{
		if( !read(fd,&buf[i],1) )
		{
			fprintf(stderr,"Error reading file into buffer.\n");
			return(-1);
		}
	}

	return(0);
}


int getPacketLen(int fd, int offset )
{
	if(lseek(fd,offset-4,SEEK_SET) == -1) // read packet len
	{
		fprintf(stderr,"Error reading out length of packet from packet header.\n");
		return(-1);
	}

	char totalBuf[4];
	if(read(fd,&totalBuf,4) == -1)
	{
		fprintf(stderr,"Error reading total number of bytes from packet header.\n");
		return(-1);
	}

	int pLen = 0;
	for(int i = 0; i < 4; i++)
	{
		pLen += totalBuf[i];
	}

	return(pLen);
}

int getTotalBytes(int fd)
{
	int totalBytes = lseek(fd,-1,SEEK_END);
	if(totalBytes < 0) {
		fprintf(stderr,"Error reading the packet header total bytes field.\n");
		return(-1);
	}

	return(totalBytes);
}
