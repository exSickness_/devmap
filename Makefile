
CFLAGS+=-std=c11
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline
CFLAGS+=-D _XOPEN_SOURCE=500

CFLAGS  +=-I./mapStructs/llist -I./mapStructs/hash -I./mapStructs/queue -I./mapStructs/heap -I./mapStructs/graph
LDFLAGS +=-L./mapStructs/llist -L./mapStructs/hash -L./mapStructs/queue -L./mapStructs/heap -L./mapStructs/graph

devmap: parseMed.o structs.o suurballes.o ./mapStructs/llist/llist.o ./mapStructs/hash/hash.o ./mapStructs/queue/queue.o ./mapStructs/heap/heap.o ./mapStructs/graph/graph.o ./mapStructs/graph/dijkstra.o -lm


.PHONY: clean debug

clean:
	-rm devmap *.o

debug: CFLAGS+=-g
debug: devmap