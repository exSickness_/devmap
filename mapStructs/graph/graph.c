
#include "graph.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


void __graph_remove_edges_from_node(struct node *v);

graph *graph_create(void)
{
	graph *g = malloc(sizeof(*g));
	if(!g) {
		return NULL;
	}
	g->nodes = NULL;

	return g;
}

void graph_disassemble(graph *g)
{
	if(!g) {
		return;
	}

	struct node *curr = g->nodes;
	while(curr) {
		struct edge *e = curr->edges;
		while(e) {
			struct edge *tmp = e->next;
			free(e);

			e = tmp;
		}

		struct node *tmp = curr->next;
		free(curr);

		curr = tmp;
	}

	free(g);
}

void graph_destroy(graph *g)
{
	if(!g) {
		return;
	}

	struct node *curr = g->nodes;
	while(curr) {
		struct edge *e = curr->edges;
		while(e) {
			struct edge *tmp = e->next;
			free(e);

			e = tmp;
		}

		struct node *tmp = curr->next;
		free((void*)curr->data);
		free(curr);

		curr = tmp;
	}

	free(g);
}

bool graph_add_node(graph *g, struct fields* data)
{
	if(!g) {
		return false;
	}

	struct node *new_node = malloc(sizeof(*new_node));
	if(!new_node) {
		return false;
	}
	new_node->data = data;
	new_node->edges = NULL;
	new_node->next = g->nodes;

	g->nodes = new_node;

	return true;
}

bool graph_add_edge(graph *g, int *from, int *to, int weight)
{
	if(!g) {
		return false;
	}
	struct node *node_from=NULL, *node_to=NULL;

	// Find the two nodes; it is allowed for them to be the same node
	struct node *curr = g->nodes;
	while(curr && !(node_from && node_to)) {
		if(curr->data->dev == *from) { // want to look at dev id
			node_from = curr;
		}
		if(curr->data->dev == *to) { // want to look at dev id
			node_to = curr;
		}
		curr = curr->next;
	}
	if(!(node_from && node_to)) {
		return false;
	}

	if(node_to->data->dev == node_from->data->dev) {
		printf("From and To are the same thing!\n");
		return(false);
	}

	// Check if an edge already exists between the two
	struct edge *check = node_from->edges;
	while(check) {
		if(check->to == node_to) { // if an edge exists, updates weight with argument
			check->weight = weight;
			return true;
		}
		check = check->next;
	}

	// Insert new edge at front of list
	struct edge *new_edge = malloc(sizeof(*new_edge));
	if(!new_edge) {
		return false;
	}
	new_edge->weight = weight;
	new_edge->to = node_to;
	new_edge->next = node_from->edges;

	node_from->edges = new_edge;

	return true;
}

bool graph_has_node(graph *g, int data)
{
	if(!g) {
		return false;
	}

	struct node *curr = g->nodes;
	while(curr) {
		if(curr->data->dev == data) {
			return true;
		}

		curr = curr->next;
	}

	return false;
}

int graph_update_node(graph *g, struct fields *data)
{
	if(!g) {
		return -1;
	}

	struct node *curr = g->nodes;
	while(curr) {
		if(curr->data->dev == data->dev) {
			break;
		}

		curr = curr->next;
	}
	if(curr)
	{
		// if(data->lat != -1) {
		if(fabs(data->lat - -1) > .0001) {
			curr->data->lat = data->lat;
		}
		// if(data->lon != -1) {
		if(fabs(data->lon - -1) > .0001) {
			curr->data->lon = data->lon;
		}
		// if(data->alt != -1) {
		if(fabs(data->alt - -1) > .0001) {
			curr->data->alt = data->alt;
		}
		// if(data->pow != -1) {
		if(fabs(data->pow - -1) > .0001) {
			curr->data->pow = data->pow;
		}
		free(data);
		return(1);
	}
	free(data);
	return -1;
}


bool graph_remove_node(graph *g, int data)
{
	if(!g) {
		return false;
	}

	bool removed = false;
	struct node *curr_node = g->nodes;
	struct node *prev_node = curr_node;
	struct node *target = NULL;

	if(!curr_node) {
		return removed;
	}
	else if(curr_node->data->dev == data) {
		g->nodes = curr_node->next;

		__graph_remove_edges_from_node(curr_node);

		target = curr_node;
		free(curr_node->data);
		free(curr_node);
		curr_node = g->nodes;
		removed = true;
	}

	while(curr_node) {
		if(curr_node->data->dev == data) {
			prev_node->next = curr_node->next;

			__graph_remove_edges_from_node(curr_node);
			target = curr_node;
			free(curr_node->data);
			free(curr_node);
			curr_node = prev_node->next;
			removed = true;
			continue;
		}

		struct edge *curr_edge = curr_node->edges;
		struct edge *prev_edge = curr_edge;

		if(!curr_edge) {
			goto NEXT_NODE;
		} else if((target && curr_edge->to == target) || curr_edge->to->data->dev == data) {
			curr_node->edges = curr_edge->next;
			free(curr_edge);
			goto NEXT_NODE;
		}

		while(curr_edge) {
			if((target && curr_edge->to == target) || curr_edge->to->data->dev == data) {
				prev_edge->next = curr_edge->next;
				free(curr_edge);
				break;
			}

			prev_edge = curr_edge;
			curr_edge = curr_edge->next;
		}

NEXT_NODE:
		prev_node = curr_node;
		curr_node = curr_node->next;
	}

	return removed;
}

bool graph_remove_edge(graph *g, int from, int to)
{
	if(!g) {
		return false;
	}

	struct node *curr = g->nodes;
	while(curr && curr->data->dev != from) {
		curr = curr->next;
	}
	if(!curr) {
		return(false);
	}

	struct edge *check = curr->edges;
	if(!check) { 						// no edges
		return false;
	} else if(check->to->data->dev == to) { // remove very first edge
		curr->edges = check->next;
		free(check);
		return true;
	}

	while(check && check->next) { 		// check curr edge and the next edge
		if(check->next->to->data->dev == to) { // see if this is the edge we want to remove
			struct edge *to_remove = check->next; // set edge equal to the target edge
			check->next = to_remove->next;			// point curr edge to the edge after target (skipping target edge)
			free(to_remove);						// remove the target
			return true;
		}

		check = check->next;
	}

	return false;
}

size_t graph_node_count(const graph *g)
{
	if(!g) {
		return 0;
	}

	size_t count = 0;
	struct node *curr = g->nodes;
	while(curr) {
		++count;
		curr = curr->next;
	}

	return count;
}

size_t graph_edge_count(const graph *g)
{
	if(!g) {
		return 0;
	}

	size_t count = 0;
	struct node *curr = g->nodes;
	while(curr) {
		struct edge *check = curr->edges;
		while(check) {
			++count;
			check = check->next;
		}
		curr = curr->next;
	}

	return count;
}

int graph_edge_weight(const graph *g, const void *from, const void *to)
{
	if(!g) {
		return 0;
	}

	struct node *curr = g->nodes;
	while(curr && curr->data->dev != *(int*)from) { // walk through nodes until you find starting node
		curr = curr->next;
	}
	if(!curr) { // didn't find starting node
		return 0;
	}

	struct edge *check = curr->edges; // create struct to walk through list of edges of target node
	while(check && check->to->data->dev != *(int*)to) { // walk nodes until find the target node
			check = check->next;
	}
	if(!check) { // didn't find edge connecting from -> to
		return 0;
	}

	return check->weight; // return edge weight from -> to
}

struct llist *graph_adjacent_to(const graph *g, const void *data)
{
	if(!g || !data) {
		return NULL;
	}

	struct node *curr = g->nodes;
	while(curr) {								// going through nodes
		if(curr->data->dev == *(int*)data) {				// found target node
			if(!curr->edges) {					// check if the target node has edges
				return NULL;
			}

			struct llist *head = NULL;			// create llist that's being returned
			struct edge *adj = curr->edges;		// create initial edge to walk through
			while(adj) {
				ll_add(&head, &adj->to->data->dev); 	// add edge
				adj = adj->next;				// get next edge
			}

			return head;
		}
		curr = curr->next;
	}
	return NULL;
}

bool is_adjacent(graph* g, int* node, int* adjTo)
{
	struct llist* adjacencies = graph_adjacent_to(g, adjTo);

	struct llist* curr = adjacencies;

	while(curr)
	{
		if(*((int*)curr->data) == *node) {
			ll_disassemble(adjacencies);
			return(true);
		}
		curr = curr->next;
	}

	ll_disassemble(adjacencies);
	return(false);
}

void graph_print(const graph *g, void to_print(int , bool is_node))
{
	if(!g) {
		return;
	}

	struct node *curr = g->nodes;
	while(curr) {
		printf("%.1f >>",curr->data->pow < 0 ? 0.0 : curr->data->pow * 100);
		to_print(curr->data->dev, true);

		struct edge *check = curr->edges;
		while(check) {
			to_print(check->to->data->dev, false);
			check = check->next;
		}

		curr = curr->next;
		printf("\n");
	}
}

void __graph_remove_edges_from_node(struct node *v)
{
	struct edge *e = v->edges;
	while(e) {
		struct edge *tmp = e->next;
		free(e);

		e = tmp;
	}
}
