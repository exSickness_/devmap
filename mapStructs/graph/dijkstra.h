#ifndef DIJKSTRA_H
	#define DIJKSTRA_H

#include "llist.h"
#include "hash.h"
#include "heap.h"
#include "queue.h"
#include "graph.h"

struct pqueue_node {
	void *data;
	int priority;
};

struct visited_node {
	int distance;
	struct pqueue_node *priority;
	void *prev;
};

graph *big_graph(void);
void print_item(int data, bool is_node);
void print_path(struct llist *path);
int pq_compare(void *a, void *b);
struct pqueue_node *__make_node(void *data, int priority);
struct visited_node *__make_vnode(int distance, struct pqueue_node *priority, void *prev);
struct llist *dijkstra_path(graph *g, void *from, void *to);
struct llist *graph_path(graph *g, void *from, void *to);
int theDriver(void);

#endif