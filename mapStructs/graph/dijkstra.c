
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

#include "llist.h"
#include "hash.h"
#include "heap.h"
#include "queue.h"
#include "graph.h"

#include "dijkstra.h"


int pq_compare(void *a, void *b)
{
	if(!a) {
		return -(intptr_t)b;
	}
	if(!b) {
		return (intptr_t)a;
	}

	struct pqueue_node *pqa, *pqb;
	pqa = (struct pqueue_node *)a;
	pqb = (struct pqueue_node *)b;

	return pqa->priority - pqb->priority;
}

struct pqueue_node *__make_node(void *data, int priority)
{
	struct pqueue_node *pqn = malloc(sizeof(*pqn));
	if(!pqn) {
		return NULL;
	}
	pqn->data = data;
	pqn->priority = priority;

	return pqn;
}



struct visited_node *__make_vnode(int distance,
		struct pqueue_node *priority, void *prev)
{
	struct visited_node *vis = malloc(sizeof(*vis));
	if(!vis) {
		return NULL;
	}
	vis->prev = prev;
	vis->distance = distance;
	vis->priority = priority;

	return vis;
}

struct llist *dijkstra_path(graph *g, void *from, void *to)
{
	heap *to_process = heap_create(pq_compare);
	struct pqueue_node *start =__make_node(from, 0); 				// create the first pqueue node. holds the first (from argument) node
	heap_add(to_process, start);

	hash *visited = hash_create(); 									// hash will hold processed nodes and the previous node
	struct visited_node *first = __make_vnode(0, start, NULL);
	hash_insert(visited, from, first); 								// insert the first ( from argument ) node
	while(!heap_is_empty(to_process)) { 							// while there is stuff in the heap to process
		struct pqueue_node *curr = heap_remove_min(to_process);		// curr is a pqueue node

		if(curr->data == to) { 										// found target
			free(curr);
			goto FOUND;
		}
		struct llist *adjacencies = graph_adjacent_to(g, curr->data); // llist of adjacent devices of curr node
		struct llist *check = adjacencies;							 // llist starting at first adjacent node
		while(check) {
			int dist = curr->priority + graph_edge_weight(g, curr->data, check->data); // current distance traveled + the next edge

			if(!hash_exists(visited, (int*)check->data) ) { 			// device hasn't been processed
				struct pqueue_node *pq_to_add =__make_node(check->data, dist); // create a pqueue node

				struct visited_node *next_node = __make_vnode(dist, pq_to_add, curr->data); // creates a visited node struct

				hash_insert(visited, check->data, next_node);		// add the visited node struct to hash with value next_node
				heap_add(to_process, pq_to_add);					// add pqueue node to heap

			} else {												// device has already been processed
				struct visited_node *found = hash_fetch(visited, check->data);

				if(dist < found->distance) {
					found->distance = dist;
					found->prev = curr->data;
					found->priority->priority = dist;
					heap_rebalance(to_process);
				}
			}

			check = check->next;
		}

		free(curr);
		ll_disassemble(adjacencies);
	}
	heap_destroy(to_process);
	hash_destroy(visited);
	return NULL;

FOUND:
	heap_destroy(to_process);

	struct llist *path = ll_create(to);
	while(((struct visited_node *)hash_fetch(visited, path->data))->prev) {
		ll_add(&path, ((struct visited_node *)hash_fetch(visited, path->data))->prev);
	}

	hash_destroy(visited);

	return path;
}

struct llist *graph_path(graph *g, void *from, void *to)
{
	hash *visited = hash_create();
	queue *to_process = queue_create();

	hash_insert(visited, from, NULL);
	queue_enqueue(to_process, from);

	while(!queue_is_empty(to_process)) {
		void *curr = queue_dequeue(to_process);

		struct llist *adjacencies = graph_adjacent_to(g, curr);
		struct llist *check = adjacencies;
		while(check) {
			if(!hash_exists(visited, check->data)) {
				hash_insert(visited, check->data, curr);
				queue_enqueue(to_process, check->data);
				if(check->data == to) {
					ll_disassemble(adjacencies);
					goto FOUND;
				}
			}

			check = check->next;
		}

		ll_disassemble(adjacencies);
	}

	queue_disassemble(to_process);
	hash_disassemble(visited);
	return NULL;

FOUND:
	queue_disassemble(to_process);

	struct llist *path = ll_create(to);
	while(hash_fetch(visited, path->data)) {
		ll_add(&path, hash_fetch(visited, path->data));
	}

	hash_disassemble(visited);

	return path;
}


void print_item(int data, bool is_node)
{
	if(is_node) {
		printf("%d", data);
	} else {
		printf(u8" → %d", data);
	}
}

void print_path(struct llist *path)
{
	while(path) {
		printf(u8"%d → ", *(int *)(path->data));
		path = path->next;
	}
	printf("\n");
}
