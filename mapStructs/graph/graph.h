#ifndef GRAPH_H
 #define GRAPH_H

#include <stdbool.h>
#include <stdlib.h>


#include "llist.h"


struct edge {
	int weight;
	struct node *to;
	struct edge *next;
};

struct fields {
	double lat;
	double lon;
	float alt;
	double pow;
	int dev;
};

struct node {
	struct fields *data;
	struct edge   *edges;
	struct node   *next;
};

struct _adjllist_graph {
	struct node *nodes;
};

typedef struct _adjllist_graph graph;



graph *graph_create(void);
void graph_disassemble(graph *g);
void graph_destroy(graph *g);

bool graph_add_node(graph *g, struct fields *data);
bool graph_add_edge(graph *g, int *from, int *to, int weight);

bool graph_has_node(graph *g, int data);
int graph_update_node(graph *g, struct fields *data);

bool graph_remove_node(graph *g, int data);
bool graph_remove_edge(graph *g, int from, int to);

size_t graph_node_count(const graph *g);
size_t graph_edge_count(const graph *g);

int graph_edge_weight(const graph *g, const void *from, const void *to);

bool is_adjacent(graph* g, int* node, int* adjTo);
struct llist *graph_adjacent_to(const graph *g, const void *data);

void graph_print(const graph *g, void to_print(int, bool is_node));

#endif
