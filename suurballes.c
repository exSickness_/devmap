
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <getopt.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "structs.h"
#include "parseMed.h"
#include "llist.h"
#include "hash.h"
#include "heap.h"
#include "queue.h"
#include "graph.h"
#include "dijkstra.h"
#include "suurballes.h"


struct llist* checkGPS(graph* g)
{
	if(!g) {
		return(NULL);
	}

	struct llist* noGPS = NULL;

	// Start checking at the first node
	struct node *curr = g->nodes;
	while(curr) {
		// Check if longitude is set
		if(fabs(curr->data->lon - -1 ) < .001) {
			if(noGPS) {
				// Add if the llist has been created
				ll_add(&noGPS, &curr->data->dev);
			}
			else {
				// Create llist
				noGPS = ll_create(&curr->data->dev);
			}
		}

		curr = curr->next;
	}

	return(noGPS);
}


struct llist* checkBattery(graph* g, int percentage, bool showEveryBat)
{
	struct llist* lowBattery = NULL;

	struct node *curr = g->nodes;
	while(curr)
	{
		if(curr->data->pow <= percentage) { // battery percentage is less than specified
			if( (fabs(curr->data->pow - -1) < .001) && !showEveryBat ) // skip if no bat packets shown
			{
				curr = curr->next;
				continue;
			}
			if(lowBattery) {
				ll_add(&lowBattery, &curr->data->dev);
			}
			else {
				lowBattery = ll_create(&curr->data->dev);
			}
		}
		curr = curr->next;
	}


	return(lowBattery);
}

struct llist* secondPath(graph* g, struct llist *path, int* from, int* to)
{
	if(!path) {
		return(NULL);
	}

	struct llist* prev = path;
	struct llist* curr = path->next;

	while(curr) { // remove edges from first run through
		graph_remove_edge(g, *((int*)prev->data), *((int*)curr->data) );
		graph_remove_edge(g, *((int*)curr->data), *((int*)prev->data) );
		prev = prev->next;
		curr = curr->next;
	}

	struct llist* path2 = dijkstra_path(g, from, to); // second pass from dijkstra

	prev = path;
	curr = path->next;

	while(curr) { // replace edges
		graph_add_edge(g, (int*)prev->data, (int*)curr->data, 1 );
		graph_add_edge(g, (int*)curr->data, (int*)prev->data, 1 );
		prev = prev->next;
		curr = curr->next;
	}

	return(path2);
}

struct llist* checkPaths(graph* g, size_t* moreThanHalf)
{
	struct llist* removeNodes = NULL;
	struct node* curr = g->nodes;

	size_t nodeCount = graph_node_count(g);
	nodeCount /= 2;

	while(curr)
	{
		struct node* tester = curr->next;
		while(tester)
		{
			if( !is_adjacent(g,&curr->data->dev,&tester->data->dev) )
			{
				struct llist *path = dijkstra_path(g, &curr->data->dev, &tester->data->dev);
				struct llist* path2 = secondPath(g,path,&curr->data->dev, &tester->data->dev);

				if( path2) {
					/* Found second path. Do nothing */;
				}
				else {
					*moreThanHalf += 1;
					if(removeNodes) {
						ll_add(&removeNodes, &curr->data->dev);
					}
					else {
						removeNodes = ll_create(&curr->data->dev);
					}
					ll_disassemble(path);
					ll_disassemble(path2);
					break;
				}

				ll_disassemble(path);
				ll_disassemble(path2);
			}
			tester = tester->next;
		}
		if(*moreThanHalf > nodeCount)
		{
			ll_disassemble(removeNodes);
			*moreThanHalf = 1;
			return(NULL);
		}
		curr = curr->next;
	}

	return(removeNodes);
}


void add_med_edges(graph *g)
{
	if(!g) {
		return;
	}

	struct node *curr = g->nodes;
	while(curr)
	{
		struct node *tester = g->nodes;
		while(tester)
		{
			if(test_distance(curr->data, tester->data) && curr->data->dev != tester->data->dev ) {
				graph_add_edge(g,&curr->data->dev,&tester->data->dev,1);
				graph_add_edge(g,&tester->data->dev,&curr->data->dev,1);
			}

			tester = tester->next;
		}
		curr = curr->next;
	}
}



bool test_distance( struct fields* curr, struct fields* tester )
{

#ifndef M_PI
	#define M_PI acos(-1.0)
#endif

	double lat1 = curr->lat;
	double lon1 = curr->lon;

	double lat2 = tester->lat;
	double lon2 = tester->lon;

	lat1 *= (M_PI / 180);
	lon1 *= (M_PI / 180);
	lat2 *= (M_PI / 180);
	lon2 *= (M_PI / 180);

	double R = 6378; // average equatorial radius in km

	double dlon = fabs(lon2 - lon1);
	double dlat = fabs(lat2 - lat1);

	double a = pow(sin(dlat/2), 2) + cos(lat1) * cos(lat2) * pow(sin(dlon/2), 2);
	double c = 2 * atan2(sqrt(a), sqrt(1-a));
	double d = (R * c);

	float dalt = fabsf((curr->alt - tester->alt) *.000305);
	double distance = sqrt( d * d + dalt * dalt);

	distance *= 1000;
	if(distance < 5.00 && distance > 0.381) {
		return(true);
	}

	return(false);
}

