#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <getopt.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "structs.h"
#include "parseMed.h"
#include "llist.h"
#include "hash.h"
#include "heap.h"
#include "queue.h"
#include "graph.h"
#include "dijkstra.h"
#include "suurballes.h"

int verifyNodes(graph* g);
int networkAlterations(graph* g);
int batteryInfo(graph* g, int percentage, bool showEveryBat);
int readFiles(int argc, char* argv[], int curFile, graph* g);
int parseArgs(int argc, char* argv[], int* curFile, int* percentage, bool* showEveryBat);


int main(int argc, char* argv[])
{
	graph* g = graph_create();

	int curFile = 1, percentage = 5;
	bool showEveryBat = false;

	if(parseArgs(argc, argv, &curFile, &percentage, &showEveryBat) == -1) {
		fprintf(stderr,"Error parsing arguments.\n");
		return(-1);
	}

    // Function to parse each file and populate graph
	if(readFiles(argc, argv, curFile, g) == -1) {
		fprintf(stderr,"Error parsing files.\n");
		return(-1);
	}

	// Verify each node and check for valid graph
	if(verifyNodes(g) == -1) {
		fprintf(stderr,"Error verifying nodes.\n");
		return(-1);
	}

	// Determine if network requires alterations
	if(networkAlterations(g) == -1) {
		fprintf(stderr,"Error with network.\n");
		return(-1);
	}

	// Extract and print battery information
	if(batteryInfo(g, percentage, showEveryBat) == -1 ) {
		fprintf(stderr,"Error extracting battery information.\n");
		return(-1);
	}

	graph_destroy(g);
	return(0);
}

int parseArgs(int argc, char* argv[], int* curFile, int* percentage, bool* showEveryBat)
{
	if(argc == 1) {
		fprintf(stderr, "Invalid number of arguments. Enter pcap files to parse.\n");
		return(-1);
	}
	// Retrieve argument values
	char* endptr = NULL;
	int option = -1;
	while ((option = getopt(argc, argv,"ap:")) != -1)
	{
        switch (option)
        {
             case 'a' : *showEveryBat = true;
                 		break;
             case 'p' : *percentage = strtol(optarg,&endptr,10);
                 		break;
             default  :	fprintf(stderr,"Invalid flags.\n");
                		return(-1);
        }
    }
    // Set the first file to parse after flags
    *curFile = optind;

    // Percentage argument not valid
    if(*percentage > 100 || *percentage < 0) {
    	fprintf(stderr,"Invalid percentage. Valid numbers are (0-100).\n");
    	return(-1);
    }

    return(0);
}

int verifyNodes(graph* g)
{
	// Check for nodes without GPS data
    struct llist* noGPS = checkGPS(g);
    struct llist* remover = noGPS;
    if(remover) {
    	printf("Devices found without GPS data:\n");
	    while(remover)
	    {
	    	printf("Device #%d\n", *(int*)remover->data);

	    	graph_remove_node(g,*(int*)remover->data);

	    	remover = remover->next;
	    }
	    printf("\n");

	    ll_disassemble(noGPS);
    }

    // Check if nodes are 0 || 1
	int nodes = graph_node_count(g);
	if( nodes == 0) {
		fprintf(stderr,"No devices found in specified .pcap files.\n");
		free(g);
		return(-1);
	}
	if( nodes == 1) {
		fprintf(stderr,"One device found in specified .pcap files.\n");
		free(g);
		return(-1);
	}

	return(0);
}

int batteryInfo(graph* g, int percentage, bool showEveryBat)
{
	// Print out all the devices below the specified percentage
	printf("\nLow Battery (%d%%):\n",percentage);
	struct llist* lowBattery = checkBattery(g,percentage, showEveryBat);
	if( lowBattery ) {
		struct llist* lowBatteryPrint = lowBattery;

		while(lowBatteryPrint) {
			printf("Device #%d\n",*( (int*)lowBatteryPrint->data ));
			lowBatteryPrint = lowBatteryPrint->next;
		}
		ll_disassemble(lowBattery);
	}
	// No devices with low battery or no battery packets
	else {
		printf("All known devices were above specified percentage or had no battery data\n");
	}

	return(0);
}

int networkAlterations(graph* g)
{
	// Connect nodes based upon distance
	add_med_edges(g);

	// Initialize values that will be used to check network alterations.. or lack thereof
	struct llist* removeNodes = NULL;
	size_t moreThanHalf = 0;

	// removeNodes will be a llist of nodes that don't have two paths to every other node
	removeNodes = checkPaths(g,&moreThanHalf);

	// Number of removals are more than half
	if( removeNodes == NULL && moreThanHalf ) {
		printf("Too many changes are needed!\n");
	}
	// Every node has two valid paths to other nodes
	else if(removeNodes == NULL) {
		printf("Network satisfies vendor recommendations!\n");
	}
	// There are nodes without two paths to every other nodes
	else
	{
		printf("Network Alterations:\n");
		struct llist* printNode = removeNodes;
		while(printNode) {
			printf("Remove device #%d\n",*( (int*)printNode->data ));
			printNode = printNode->next;
		}
		ll_disassemble(removeNodes);
	}
	return(0);
}

int readFiles(int argc, char* argv[], int curFile, graph* g)
{
	size_t sz = 200;
	unsigned int* buf = calloc(sz,sizeof(*buf));
	if(!buf) {
		fprintf(stderr,"Error allocating memory for the buffer.\n");
		return(-1);
	}

	for( ; curFile < argc; curFile++ )
	{
		int totalBytes = 1, fd = -1;

		fd = open(argv[curFile],O_RDONLY);
		if(fd == -1) {
			fprintf(stderr,"Unable to open file.\n");
			continue;
		}

		// Get the total amount of bytes in the file
		totalBytes = getTotalBytes(fd);
		if(totalBytes < 0) {
			fprintf(stderr,"Error reading file.\n");
			goto nextFile;
		}

		// Make temporary skeleton structure on stack
		struct meditrik mHeader;
		struct meditrik* mPoint = &mHeader;

		int offset = 0;
		offset += 24;
		// Loop to read each packet in the file
		while( offset < totalBytes )
		{
			// Memset structures that hold changing data
			memset(mPoint,0,sizeof(struct meditrik));
			memset(buf,0,sz);

			// Get the total packet length from the packet header
			offset += 16;
			int pLen = getPacketLen(fd,offset);

			// Read one packet into the buffer
			readPacket(fd, buf, offset, pLen);

			// Move to the start of the meditrik header depending upon the protocols
			int medStart = getMedStart( checkProtocol(buf) );
			if( medStart == -1 ) {
				fprintf(stderr,"Error finding meditrik header.\n");
				goto nextPacket;
			}

			// Check for correct meditrik header
			if( getMedHead(buf,mPoint,medStart) == -1) {
				fprintf(stderr,"Error reading meditrik header.\n");
				goto nextPacket;
			}

			// Create a fields structure that will hold the device information
			struct fields* f = createField();
			if(!f) {
				fprintf(stderr,"Error allocating memory.\n");
				goto nextPacket;
			}

			f->dev = mPoint->sourceID;

			// Check for correct meditrik payload
			if( getMedType(buf,mPoint,medStart, f) == -1 ) {
				fprintf(stderr,"Error reading meditrik data.\n");
				free(f);
				goto nextPacket;
			}

			// Check if the device is already in the graph
			if( graph_has_node(g, f->dev) ) {
				// Update the node if it exists
				graph_update_node(g, f);
			}
			else {
				// Create a new node should it not exist
				graph_add_node(g, f);
			}

			nextPacket:

			// Set the offset to after the current packet
			offset += pLen;
		}

		nextFile:

		close(fd);
	}

	free(buf);

	return(0);
}
