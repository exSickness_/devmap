#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <ctype.h>

#include "structs.h"

int initGlobal(struct global* gHead)
{
	if(gHead== NULL)
	{
		fprintf(stderr,"Null parameter passed in.\n");
		return(-1);
	}
	gHead->magicNum =0xf;
	gHead->magicNum = htonl(gHead->magicNum);

	gHead->majVer =0xf;
	gHead->majVer = htonl(gHead->majVer);

	gHead->minVer =0xf;
	gHead->minVer = htonl(gHead->minVer);
	
	gHead->gmtTime =0xf;
	gHead->gmtTime = htonl(gHead->gmtTime);

	gHead->accuracy =0xf;
	gHead->accuracy = htonl(gHead->accuracy);

	gHead->snapLen =0xf;
	gHead->snapLen = htonl(gHead->snapLen);

	gHead->linkLayer =0xf;
	gHead->linkLayer = htonl(gHead->linkLayer);

	return(0);
}

int initPktHead(struct pktHead* pHead)
{
	if(pHead== NULL)
	{
		fprintf(stderr,"Null parameter passed in.\n");
		return(-1);
	}
	pHead->secs =0xe;
	pHead->secs = htonl(pHead->secs);

	pHead->micro =0xe;
	pHead->micro = htonl(pHead->micro);

	pHead->lenBytes =0xe;
	pHead->lenBytes = htonl(pHead->lenBytes);

	pHead->lenWire =0xe;
	pHead->lenWire = htonl(pHead->lenWire);

	return(0);
}

int initEthHead(struct ethernet* eHead)
{
	if(eHead== NULL)
	{
		fprintf(stderr,"Null parameter passed in.\n");
		return(-1);
	}
	eHead->destMac1 =0xd;
	eHead->destMac1 = htonl(eHead->destMac1);

	eHead->destMac2 =0xd;
	eHead->destMac2= htonl(eHead->destMac2);

	eHead->sourceMac1 =0xd;
	eHead->sourceMac1= htonl(eHead->sourceMac1);

	eHead->sourceMac2 =0xd;
	eHead->sourceMac2= htonl(eHead->sourceMac2);

	eHead->nextType =0xd;
	eHead->nextType= htonl(eHead->nextType);

	return(0);
}

int initIpHead(struct ipv4* ipHead)
{
	if(ipHead== NULL)
	{
		fprintf(stderr,"Null parameter passed in.\n");
		return(-1);
	}
	ipHead->ipv41 =0xc;
	ipHead->ipv41= htonl(ipHead->ipv41);

	ipHead->ipv42 =0xc;
	ipHead->ipv42= htonl(ipHead->ipv42);

	ipHead->ipv43 =0xc;
	ipHead->ipv43= htonl(ipHead->ipv43);

	ipHead->ipv44 =0xc;
	ipHead->ipv44= htonl(ipHead->ipv44);

	ipHead->ipv45 =0xc;
	ipHead->ipv45= htonl(ipHead->ipv45);
	return(0);
}

int initUdpHead(struct udp* uHead)
{
	if(uHead== NULL)
	{
		fprintf(stderr,"Null parameter passed in.\n");
		return(-1);
	}
	
	uHead->sourceP =0xa;
	uHead->sourceP= htons(uHead->sourceP);

	uHead->destP =0xa;
	uHead->destP= htons(uHead->destP);

	uHead->len =0xa;
	uHead->len= htons(uHead->len);

	uHead->checksum =0xa;
	uHead->checksum= htons(uHead->checksum);

	return(0);
}
