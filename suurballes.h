#ifndef SUURBALLES_H
	#define SUURBALLES_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <getopt.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "structs.h"
#include "parseMed.h"
#include "llist.h"
#include "hash.h"
#include "heap.h"
#include "queue.h"
#include "graph.h"
#include "dijkstra.h"


struct llist* checkGPS(graph* g);
struct llist* checkBattery(graph* g, int percentage, bool showEveryBat);
struct llist* secondPath(graph* g, struct llist *path, int* from, int* to);
struct llist* checkPaths(graph* g, size_t* moreThanHalf);
void add_med_edges(graph *g);
bool test_distance( struct fields* curr, struct fields* tester );


#endif